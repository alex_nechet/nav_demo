package com.example.alexn.navigationdemo.ui.summary

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.alexn.navigationdemo.R
import com.example.alexn.navigationdemo.databinding.FragmentSummaryBinding

class SummaryFragment : Fragment() {
    private lateinit var viewModel: SummaryViewModel
    private var binding: FragmentSummaryBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_summary, container, false)
        binding!!.setLifecycleOwner(this)
        viewModel = ViewModelProviders.of(this).get(SummaryViewModel::class.java)
        binding!!.viewModel = viewModel
        return binding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val email = SummaryFragmentArgs.fromBundle(arguments).email
        val password = SummaryFragmentArgs.fromBundle(arguments).password
        viewModel.setData(Pair(first = email, second = password))
        viewModel.finishEvent.observe(this, Observer {
            view?.let { it1 -> Snackbar.make(it1, " Registration finished!", Snackbar.LENGTH_LONG).show() }
        })
    }

    }