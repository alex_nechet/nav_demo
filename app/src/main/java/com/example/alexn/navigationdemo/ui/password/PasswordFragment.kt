package com.example.alexn.navigationdemo.ui.password

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController

import com.example.alexn.navigationdemo.R
import com.example.alexn.navigationdemo.databinding.FragmentEmailBinding
import com.example.alexn.navigationdemo.databinding.FragmentPasswordBinding
import com.example.alexn.navigationdemo.ui.email.EmailViewModel

class PasswordFragment : Fragment() {
    private lateinit var viewModel: PasswordViewModel
    private var binding: FragmentPasswordBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_password, container, false)
        binding!!.setLifecycleOwner(this)
        viewModel = ViewModelProviders.of(this).get(PasswordViewModel::class.java)
        binding!!.viewModel = viewModel
        return binding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.userDataLiveData.observe(this, Observer {
            val email = PasswordFragmentArgs.fromBundle(arguments).email
            val action = PasswordFragmentDirections.actionGlobalFragmentSummary(email, it!!)
            findNavController().navigate(action)
        })

    }

}
