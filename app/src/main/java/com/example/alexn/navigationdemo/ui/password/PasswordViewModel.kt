package com.example.alexn.navigationdemo.ui.password

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField
import com.example.alexn.navigationdemo.util.SingleLiveEvent

class PasswordViewModel : ViewModel() {
    val password: ObservableField<String> = ObservableField("")
    val userDataLiveData = SingleLiveEvent<String>()

    fun onNextClick() = Runnable { userDataLiveData.value = password.get() }
}
