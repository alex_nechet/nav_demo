package com.example.alexn.navigationdemo.ui.email

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import com.example.alexn.navigationdemo.util.SingleLiveEvent

class EmailViewModel : ViewModel() {

    val email: ObservableField<String> = ObservableField("")
    val emailLiveData = SingleLiveEvent<String>()

    fun onNextClick() = Runnable {
        emailLiveData.value = email.get() }

}
