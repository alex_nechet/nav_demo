package com.example.alexn.navigationdemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.example.alexn.navigationdemo.ui.email.EmailFragment
import kotlinx.android.synthetic.main.navigation_activity.*

class NavigationActivity : AppCompatActivity() {
    private var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.navigation_activity)
        navController = Navigation.findNavController(this, R.id.host_fragment)
    }

//    override fun onBackPressed() {
//        onSupportNavigateUp()
//    }

    override fun onSupportNavigateUp(): Boolean {
        navController?.navigateUp()
        return true
    }

}
