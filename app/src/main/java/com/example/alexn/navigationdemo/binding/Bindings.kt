package com.example.alexn.navigationdemo.binding

import android.databinding.BindingAdapter
import android.databinding.BindingConversion
import android.view.View

@BindingConversion
fun toOnClickListener(listener: Runnable?): View.OnClickListener? {
    return if (listener != null) {
        View.OnClickListener {
            try {
                listener.run()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    } else null

}

@BindingAdapter("android:visibility")
fun setVisibility(view: View, visible: Boolean?) {
    view.visibility = if (visible == true) View.VISIBLE else View.GONE
}

