package com.example.alexn.navigationdemo.ui.email

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.alexn.navigationdemo.R
import com.example.alexn.navigationdemo.databinding.FragmentEmailBinding
import kotlinx.android.synthetic.main.navigation_activity.*

class EmailFragment : Fragment() {
    private lateinit var viewModel: EmailViewModel
    private var binding: FragmentEmailBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_email, container, false)
        binding!!.setLifecycleOwner(this)
        viewModel = ViewModelProviders.of(this).get(EmailViewModel::class.java)
        binding!!.viewModel = viewModel
        return binding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.emailLiveData.observe(this, Observer {
//            findNavController().navigate(R.id.action_fragmentEmail_to_fragmentPassword)

            val action = EmailFragmentDirections.actionFragmentEmailToFragmentPassword(it!!)
            findNavController().navigate(action)
        })
    }

}
