package com.example.alexn.navigationdemo.ui.summary

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField
import android.os.Handler
import com.example.alexn.navigationdemo.util.SingleLiveEvent

class SummaryViewModel : ViewModel() {
    val finishEvent = SingleLiveEvent<Void>()
    val email: ObservableField<String> = ObservableField("")
    val password: ObservableField<String> = ObservableField("")
    val loading = MediatorLiveData<Boolean>()

    init {
        loading.value = false
    }

    fun onFinishClick() = Runnable { emulateNetwork() }

    private fun emulateNetwork() {
        loading.value = true
        Handler().postDelayed({
            loading.value = false

            // notify UI
            finishEvent.call()
        }, 1000)

    }

    fun setData(pair: Pair<String, String>) {
        email.set(pair.first)
        password.set(pair.second)
    }


}
